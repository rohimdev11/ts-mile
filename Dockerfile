FROM node:16.0.0-alpine

WORKDIR /app
COPY ./build/. ./
COPY ./package.json ./

RUN npm install
CMD ["node", "./app.js"]

EXPOSE 5015