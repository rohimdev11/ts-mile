# Technical Test

## Installation

Clone project.

```bash
git clone
```

Import change env.

```bash
NODE_ENV=dev
CORS=*
SERVER_DISABLE=false
MONGO_URI=mongodb://localhost:27017/mileApp
APP_HOST=
APP_PROTOCOL=http
SERVER_PORT=1010
```

Open and run this docs in postman

see documentation in here ([Postman](https://documenter.getpostman.com/view/3483861/2s935hQmkn))

## Usage

```
npm run dev

```

## About

Thank you.
