import cors from 'cors';
import dotenv from 'dotenv';
import express from 'express';
import helmet from 'helmet';
import http from 'http';
import config from './configs/config';
import baseRoute from './routes/base';
import Logger from './services/logger';
import createResponse from './utils/http-response';
dotenv.config({ path: process.argv[2] });

const app: express.Application = express();
const server: http.Server = http.createServer(app);

/** Secure Express HTTP headers */
app.use(helmet());

/** Connect to Mongo */
require('./configs/db');

/** Parse the body of the request */
app.use(cors({ origin: config.server.cors }));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

/** Rules of our API */
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

    if (req.method == 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }

    next();
});

app.use(`${config.server.publicUrl}/health-check`, (req, res) => {
    res.json({
        message: 'Health check success'
    })
});
app.use(`${config.server.publicUrl}/v0`, baseRoute);

/** Error handling */
app.use((req, res, next) => {
    createResponse(res, 404, {
        message: { id: 'Route Not found' }
    });
    return;
});

if (config.server.disable == true) {
    Logger.debug(`Server is running on lambda`);
} else {
    server.listen(config.server.port, () => {
        Logger.debug(`Server is running ${config.server.hostname}:${config.server.port}`);
    });
}

module.exports = app;
