const MONGO = {
    options: {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false,
        poolSize: 5
    },
    url: process.env.MONGO_URI || 'mongodb://localhost:27017/mileApp'
};

const SERVER = {
    hostname: process.env.SERVER_HOSTNAME || 'localhost',
    port: process.env.SERVER_PORT || 1010,
    cors: process.env.CORS || '*',
    publicUrl: '/api/package',
    disable: process.env.SERVER_DISABLE || false,
    appHost: process.env.APP_HOST,
    appProtocol: process.env.APP_PROTOCOL,
};

const config = {
    env: process.env.NODE_ENV || 'dev',
    debugMode: 'true',
    mongo: MONGO,
    server: SERVER,
};

export default config;
