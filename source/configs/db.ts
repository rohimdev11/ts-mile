import { Connote, ConnoteModel } from '../models/connote';
import { Koli, KoliModel } from '../models/koli';
import { Package, PackageModel } from '../models/package';
import Logger from '../services/logger';
import config from './config';
import { connect, connection, Connection } from 'mongoose';

export interface IModels {
    Package: PackageModel;
    Connote: ConnoteModel
    Koli: KoliModel
}

export class DB {
    private static instance: DB;

    private _db: Connection;
    private _models: IModels;

    private constructor() {
        connect(config.mongo.url, config.mongo.options);
        this._db = connection;
        this._db.on('open', this.connected);
        this._db.on('error', this.error);

        this._models = {
            Package: new Package().model,
            Connote: new Connote().model,
            Koli: new Koli().model
        };
    }

    public static get Models() {
        if (!DB.instance) {
            DB.instance = new DB();
        }
        return DB.instance._models;
    }

    private connected() {
        Logger.debug('Mongo Connected');
    }

    private error(error: any) {
        Logger.error(error);
    }
}
