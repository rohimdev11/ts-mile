import { NextFunction, Request, Response } from 'express';
import { BaseController } from './base-controller';
import Logger from '../services/logger';

class HealthCheckController extends BaseController {
    private message: String;

    constructor() {
        super();
        this.message = 'pong';
    }

    public handle = (req: Request, res: Response, next: NextFunction) => {
        Logger.debug('health check');
        return this.ok(res, {
            message: this.message
        });
    };
}

export default new HealthCheckController();
