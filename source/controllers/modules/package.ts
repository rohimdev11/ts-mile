import { Request, Response } from 'express';
import { IPackage } from '../../interfaces/modules/package';
import { BaseController } from '../base-controller';
import PackageService from '../../services/modules/package';
import ConnoteService from '../../services/modules/connote';
import { IConnote } from '../../interfaces/modules/connote';

class PackageController extends BaseController {
  private _package: IPackage;
  private _connote: IConnote

  constructor() {
    super();
    this._package = <IPackage>{};
    this._connote = <IConnote>{}
  }

  public getAllPackage = async (req: Request, res: Response) => {
    let { page = 1, limit = 10, search, sort } = req.query;

    const options = {
      page,
      search,
      limit,
      sortBy: sort,
      collation: {
        locale: 'en'
      }
    };
    let data = await PackageService.search(options);

    this.ok(res, data);
  };

  public create = async (req: Request, res: Response) => {
    var { customerName = "", customerCode = "",
      transactionAmount = "",
      transactionDiscount = "",
      transactionAdditionalField = "",
      transactionPaymentType = "",
      transactionStatus = "",
      transactionCode = "",
      transactionOrder = 0,
      locationId = "",
      organizationId = 0,
      transactionPaymentTypeName = "",
      transactionCashAmount = 0,
      transactionCashChange = 0,
      connote = {
        connoteNumber: 0,
        connoteService: "",
        connoteServicePrice: 0,
        connoteAmount: 0,
        connoteCode: "",
        connoteBookingCode: "",
        connoteOrder: 0,
        connoteState: "",
        connoteStateId: 0,
        zoneCodeFrom: "",
        zoneCodeTo: "",
        surchargeAmount: "",
        actualWeight: 0,
        volumeWeight: 0,
        chargeableWeight: 0,
        organizationId: 0,
        locationId: "",
        connoteTotalPackage: "",
        connoteSurchargeAmount: "",
        connoteSlaDay: "",
        locationName: "",
        locationType: "",
        sourceTariffDb: "",
        idSourceTarif: "",
        pod: "",
        history: []
      },
      customerAttribute = {
        salesName: "",
        top: "",
        customerType: "",
      },
      originData = {
        customerName: "",
        customerAddress: "",
        customerEmail: "",
        customerPhone: "",
        customerAddressDetail: "",
        customerZipCode: "",
        zoneCode: "",
        organizationId: 6,
        locationId: "",
      },
      destinationData = {
        customerName: "",
        customerAddress: "",
        customerEmail: "",
        customerPhone: "",
        customerAddressDetail: "",
        customerZipCode: "",
        zoneCode: "",
        organizationId: 6,
        locationId: "",
      },
      customField = {
        additionalNote: "",
      },
      currentLocation = {
        name: "",
        code: "",
        type: "",
      }
    } = req.body;

    this._package = <IPackage>{};
    this._package.customerName = customerName
    this._package.customerCode = customerCode
    this._package.transactionAmount = transactionAmount
    this._package.transactionDiscount = transactionDiscount
    this._package.transactionAdditionalField = transactionAdditionalField
    this._package.transactionPaymentType = transactionPaymentType
    this._package.transactionStatus = transactionStatus
    this._package.transactionCode = transactionCode
    this._package.transactionOrder = transactionOrder
    this._package.locationId = locationId
    this._package.organizationId = organizationId
    this._package.transactionPaymentTypeName = transactionPaymentTypeName
    this._package.transactionCashAmount = transactionCashAmount
    this._package.transactionCashChange = transactionCashChange
    this._package.customerAttribute = customerAttribute
    this._package.originData = originData
    this._package.destinationData = destinationData
    this._package.customField = customField
    this._package.currentLocation = currentLocation

    const createTransaction = await PackageService.create(this._package)
    if (!createTransaction) {
      return this.badRequest(res, "Server error")
    }

    this._connote = <IConnote>{}
    this._connote.packageId = createTransaction._id
    this._connote.connoteNumber = connote.connoteNumber
    this._connote.connoteService = connote.connoteService
    this._connote.connoteServicePrice = connote.connoteServicePrice
    this._connote.connoteAmount = connote.connoteAmount
    this._connote.connoteCode = connote.connoteCode
    this._connote.connoteBookingCode = connote.connoteBookingCode
    this._connote.connoteOrder = connote.connoteOrder
    this._connote.connoteState = connote.connoteState
    this._connote.connoteStateId = connote.connoteStateId
    this._connote.zoneCodeFrom = connote.zoneCodeFrom
    this._connote.zoneCodeTo = connote.zoneCodeTo
    this._connote.surchargeAmount = connote.surchargeAmount
    this._connote.actualWeight = connote.actualWeight
    this._connote.volumeWeight = connote.volumeWeight
    this._connote.chargeableWight = connote.chargeableWight
    this._connote.organizationId = connote.organizationId
    this._connote.locationId = connote.locationId
    this._connote.connoteTotalPackage = connote.connoteTotalPackage
    this._connote.connoteSurchargeAmount = connote.connoteSurchargeAmount
    this._connote.connoteSlaDay = connote.connoteSlaDay
    this._connote.locationName = connote.locationName
    this._connote.locationType = connote.locationType
    this._connote.sourceTariffDb = connote.sourceTariffDb
    this._connote.idSourceTarif = connote.idSourceTarif

    await ConnoteService.create(this._connote)

    return this.ok(res, { id: createTransaction._id });
  };

  public updatePackage = async (req: Request, res: Response) => {
    const { packageId } = req.params
    var {
      customerName = "",
      transactionStatus = "",
      customerAttribute = {
        salesName: "",
        top: "",
        customerType: "",
      },
      originData = {
        customerName: "",
        customerAddress: "",
        customerEmail: "",
        customerPhone: "",
        customerAddressDetail: "",
        customerZipCode: "",
        zoneCode: "",
        organizationId: 6,
        locationId: "",
      },
      destinationData = {
        customerName: "",
        customerAddress: "",
        customerEmail: "",
        customerPhone: "",
        customerAddressDetail: "",
        customerZipCode: "",
        zoneCode: "",
        organizationId: 6,
        locationId: "",
      },
      currentLocation = {
        name: "",
        code: "",
        type: "",
      }
    } = req.body;

    let packageData = await PackageService.findOne({ _id: packageId });
    if (!packageData) {
      return this.badRequest(res, "Package not found")
    }
    packageData.customerName = customerName
    packageData.transactionStatus = transactionStatus
    packageData.customerAttribute = customerAttribute
    packageData.originData = originData
    packageData.destinationData = destinationData
    packageData.currentLocation = currentLocation

    const updateTransaction = await PackageService.update(packageData._id, packageData)
    if (!updateTransaction) {
      return this.badRequest(res, "Server error")
    }

    return this.ok(res, { id: updateTransaction._id });
  };

  public deletePackage = async (req: Request, res: Response) => {
    let { packageId } = req.params;
    let packageData = await PackageService.findOne({ _id: packageId });
    if (!packageData._id) {
      return this.notFound(res, "Package ID is not found");
    }

    packageData.isDeleted = true

    let update = await PackageService.update(packageData._id, packageData)
    if (!update) {
      return this.badRequest(res, "Server error")
    }
    return this.ok(res);
  };

}

export default new PackageController();
