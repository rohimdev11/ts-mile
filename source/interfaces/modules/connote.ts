import { Document, ObjectId } from 'mongoose';

export interface IConnote extends Document {
  packageId: ObjectId;
  connoteNumber: number
  connoteService: string
  connoteServicePrice: number
  connoteAmount: number
  connoteCode: string
  connoteBookingCode: string
  connoteOrder: number
  connoteState: string
  connoteStateId: number
  zoneCodeFrom: string
  zoneCodeTo: string
  surchargeAmount: number
  actualWeight: number
  volumeWeight: number
  chargeableWight: number
  createdAt: Date
  updatedAt: Date
  organizationId: number
  locationId: string
  connoteTotalPackage: string
  connoteSurchargeAmount: string
  connoteSlaDay: string
  locationName: string
  locationType: string
  sourceTariffDb: string
  idSourceTarif: string
  pod: string,
  history: Array<string>
  isDeleted: Boolean
}
