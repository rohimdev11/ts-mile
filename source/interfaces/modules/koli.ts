import { Document, ObjectId } from 'mongoose';

export interface IKoli extends Document {
  packageId: ObjectId
  koliLength: number
  awbUrl: string
  koliChargeableWeight: number
  koliWidth: number
  koliSurcharge: Array<string>,
  koliHeight: number
  createdAt: Date
  updatedAt: Date
  koliDescription: string
  koliFormulaId: string,
  koliVolume: number
  koliWeight: number
  koliCustomField: Object
  koliCode: string
  isDeleted: Boolean
}
