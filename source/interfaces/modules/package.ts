import { Document } from 'mongoose';

export interface IPackage extends Document {
  customerName: string
  customerCode: string
  transactionAmount: string
  transactionDiscount: string
  transactionAdditionalField: string
  transactionPaymentType: string
  transactionStatus: string
  transactionCode: string
  transactionOrder: number
  locationId: string
  organizationId: number
  createdAt: Date
  updatedAt: Date
  transactionPaymentTypeName: string
  transactionCashAmount: number
  transactionCashChange: number
  customerAttribute: Object
  originData: Object
  destinationData: Object
  customField: Object
  currentLocation: Object
  isDeleted: Boolean
}
