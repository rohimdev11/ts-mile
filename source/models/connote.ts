import { Schema, model, Document, Model } from 'mongoose';

import { IConnote } from '../interfaces/modules/connote';
import { body } from 'express-validator';
import mongooseAggregatePaginate from 'mongoose-aggregate-paginate-v2';
export interface ConnoteModel extends Model<IConnote> {
  [x: string]: any;
}

export class Connote {
  private _model: Model<IConnote>;
  constructor() {
    const schema = new Schema(
      {
        packageId: {
          type: Schema.Types.ObjectId,
          ref: 'package',
          required: true
        },
        connoteNumber: { type: Number },
        connoteService: { type: String },
        connoteServicePrice: { type: String },
        connoteAmount: { type: Number },
        connoteCode: { type: String },
        connoteBookingCode: { type: String },
        connoteOrder: { type: Number },
        connoteState: { type: String },
        connoteStateId: { type: Number },
        zoneCodeFrom: { type: String },
        zoneCodeTo: { type: String },
        surchargeAmount: { type: Number },
        actualWeight: { type: Number },
        volumeWeight: { type: Number },
        chargeableWight: { type: String },
        organizationId: { type: Number },
        locationId: { type: String },
        connoteTotalPackage: { type: String },
        connoteSurchargeAmount: { type: String },
        connoteSlaDay: { type: String },
        locationName: { type: String },
        locationType: { type: String },
        sourceTariffDb: { type: String },
        idSourceTarif: { type: String },
        isDeleted: { type: Boolean, default: false }
      },
      { collection: 'connote', timestamps: { updatedAt: 'updatedDate', createdAt: 'createdDate' } }
    );
    schema.plugin(mongooseAggregatePaginate);
    this._model = model<IConnote>('connote', schema);
  }
  public get model(): Model<IConnote> {
    return this._model;
  }
}
