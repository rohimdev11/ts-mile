import { Model, Schema, model } from 'mongoose';

import { body } from 'express-validator';
import mongooseAggregatePaginate from 'mongoose-aggregate-paginate-v2';
import { IKoli } from '../interfaces/modules/koli';
export interface KoliModel extends Model<IKoli> {
  [x: string]: any;
}

export class Koli {
  private _model: Model<IKoli>;
  constructor() {
    const schema = new Schema(
      {
        packageId: {
          type: Schema.Types.ObjectId,
          ref: 'package',
          required: true
        },
        koliLength: { type: Number },
        awbUrl: { type: String },
        koliChargeableWeight: { type: Number },
        koliWidth: { type: Number },
        koliHeight: { type: Number },
        koliDescription: { type: String },
        koliFormulaId: { type: String },
        koliVolume: { type: String },
        koliWeight: { type: String },
        koliCustomField: {
          awbSicepat: { type: String },
          hargaBarang: { type: String }
        },
        koliCode: { type: String },
        isDeleted: { type: Boolean, default: false }
      },
      { collection: 'koli', timestamps: { updatedAt: 'updatedDate', createdAt: 'createdDate' } }
    );
    schema.plugin(mongooseAggregatePaginate);
    this._model = model<IKoli>('koli', schema);
  }
  public get model(): Model<IKoli> {
    return this._model;
  }

}
