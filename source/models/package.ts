import { Schema, model, Document, Model } from 'mongoose';

import { IPackage } from '../interfaces/modules/package';
import { body } from 'express-validator';
import mongooseAggregatePaginate from 'mongoose-aggregate-paginate-v2';
export interface PackageModel extends Model<IPackage> {
  [x: string]: any;
}

export class Package {
  private _model: Model<IPackage>;
  constructor() {
    const schema = new Schema(
      {
        customerName: { type: String },
        customerCode: { type: String },
        transactionAmount: { type: String },
        transactionDiscount: { type: String },
        transactionAdditionalField: { type: String },
        transactionPaymentType: { type: String },
        transactionStatus: { type: String },
        transactionCode: { type: String },
        transactionOrder: { type: Number, default: 0 },
        locationId: { type: String },
        orgatizationId: { type: Number, default: 0 },
        transactionPaymentTypeName: { type: String },
        transactionCashAmount: { type: Number, default: 0 },
        transactionCashChange: { type: Number, default: 0 },
        customerAttribute: {
          SalesName: { type: String },
          TOP: { type: String },
          CustomerType: { type: String }
        },
        originData: {
          customerName: { type: String },
          customerAddress: { type: String },
          customerEmail: { type: String },
          customerPhone: { type: String },
          customerAddressDetail: { type: String },
          customerZipCode: { type: String },
          zoneCode: { type: String },
          organizationId: { type: Number },
          locationId: { type: String },
        },
        destinationData: {
          customerName: { type: String },
          customerAddress: { type: String },
          customerEmail: { type: String },
          customerPhone: { type: String },
          customerAddressDetail: { type: String },
          customerZipCode: { type: String },
          zoneCode: { type: String },
          organizationId: { type: Number },
          locationId: { type: String },
        },
        customField: {
          additionalNote: { type: String }
        },
        currentLocation: {
          name: { type: String },
          code: { type: String },
          type: { type: String },
        },
        isDeleted: { type: Boolean, default: false }
      },
      { collection: 'package', timestamps: { updatedAt: 'updatedDate', createdAt: 'createdDate' } }
    );
    schema.plugin(mongooseAggregatePaginate);
    this._model = model<IPackage>('package', schema);
  }
  public get model(): Model<IPackage> {
    return this._model;
  }

}
