import { Router } from 'express';
import packageRouter from './modules/package';

const router = Router();
router.use(packageRouter)
export default router;
