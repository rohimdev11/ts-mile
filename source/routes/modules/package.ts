import { Router } from 'express';
import createResponse from '../../utils/http-response';
import PackageController from '../../controllers/modules/package';

const router = Router();
const modules: string = 'package';

const use =
  (fn: any): any =>
    (req: any, res: any, next: any) => {
      Promise.resolve(fn(req, res, next)).catch((err) => {
        createResponse(res, 500, { error: err });
      });
    };

router.get(`/${modules}`, use(PackageController.getAllPackage));
router.post(`/${modules}`, use(PackageController.create));
router.delete(`/${modules}/:packageId`, use(PackageController.deletePackage));
router.put(`/${modules}/:packageId`, use(PackageController.updatePackage));

export default router;
