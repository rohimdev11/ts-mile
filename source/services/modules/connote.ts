import { IConnote } from '../../interfaces/modules/connote';
import { DB, IModels } from '../../configs/db';
import { FilterQuery, UpdateQuery, Schema, Types } from 'mongoose';
type ID = Schema.Types.ObjectId;

interface IConnoteServices {
  findOne(params: any): Promise<IConnote>;
  create(data: IConnote): Promise<IConnote>;
  update(_id: ID, data: IConnote): Promise<IConnote>;
  delete(_id: ID, data: IConnote): Promise<IConnote>;
  findByIdAndUpdate(_id: string, data: IConnote): Promise<IConnote>;
}

class PackageServices implements IConnoteServices {
  private db: IModels;

  constructor() {
    this.db = DB.Models;
  }

  public findAll = async (params: any): Promise<Array<IConnote>> => {
    return (await this.db.Connote.find(params).exec()) || new Array<IConnote>();
  };

  public findOne = async (params: any): Promise<IConnote> => {
    return (
      (await this.db.Connote.findOne(params)
        .exec()) || <IConnote>{}
    );
  };

  public create = async (data: IConnote): Promise<IConnote> => {
    return await this.db.Connote.create(data);
  };

  public update = async (_id: ID, data: IConnote): Promise<IConnote> => {
    const existing = await this.db.Connote.findOneAndUpdate({ _id: _id }, data);
    return existing || <IConnote>{};
  };

  public updateMany = async (filter: FilterQuery<IConnote>, set: UpdateQuery<IConnote>): Promise<void> => {
    await this.db.Connote.updateMany(filter, set);
  };

  public delete = async (_id: any): Promise<any> => {
    return await this.db.Connote.findOneAndDelete({ _id: _id });
  };

  public findByIdAndUpdate = async (_id: string, data: IConnote): Promise<IConnote> => {
    return await this.findByIdAndUpdate(_id, data);
  };
}

export default new PackageServices();
