import { IPackage } from '../../interfaces/modules/package';
import { DB, IModels } from '../../configs/db';
import { FilterQuery, UpdateQuery, Schema, Types } from 'mongoose';
type ID = Schema.Types.ObjectId;

interface IPackageServices {
  findOne(params: any): Promise<IPackage>;
  create(data: IPackage): Promise<IPackage>;
  update(_id: ID, data: IPackage): Promise<IPackage>;
  delete(_id: ID, data: IPackage): Promise<IPackage>;
  findByIdAndUpdate(_id: string, data: IPackage): Promise<IPackage>;
}

class PackageServices implements IPackageServices {
  private db: IModels;

  constructor() {
    this.db = DB.Models;
  }

  public findAll = async (params: any): Promise<Array<IPackage>> => {
    return (await this.db.Package.find(params).exec()) || new Array<IPackage>();
  };

  public findOne = async (params: any): Promise<IPackage> => {
    return (
      (await this.db.Package.findOne(params)
        .populate({ path: 'package', populate: { path: 'connote' } })
        .exec()) || <IPackage>{}
    );
  };

  public create = async (data: IPackage): Promise<IPackage> => {
    return await this.db.Package.create(data);
  };

  public update = async (_id: ID, data: IPackage): Promise<IPackage> => {
    const existing = await this.db.Package.findOneAndUpdate({ _id: _id }, data);
    return existing || <IPackage>{};
  };

  public updateMany = async (filter: FilterQuery<IPackage>, set: UpdateQuery<IPackage>): Promise<void> => {
    await this.db.Package.updateMany(filter, set);
  };

  public delete = async (_id: any): Promise<any> => {
    return await this.db.Package.findOneAndDelete({ _id: _id });
  };

  public findByIdAndUpdate = async (_id: string, data: IPackage): Promise<IPackage> => {
    return await this.findByIdAndUpdate(_id, data);
  };

  public getAlPackage = async (opts: any): Promise<any> => {
    const options = {
      page: opts.page ? opts.page : 1,
      limit: opts.limit ? (opts.limit > 50 ? 50 : opts.limit) : 10
    };

    const aggregate = this.db.Package.aggregate([
      {
        $match: {
          isDeleted: false,
        }
      },
      {
        $lookup: {
          from: 'connote',
          localField: 'packageId',
          foreignField: '_id',
          as: 'connote'
        }
      },
      {
        $unwind: {
          path: '$connote',
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: 'koli',
          localField: 'pacakgeId',
          foreignField: '_id',
          as: 'koliData'
        }
      },
    ]);

    return await this.db.Package.aggregatePaginate(aggregate, options);
  };

  public search = async (opts: any): Promise<any> => {
    let search = typeof opts.search === 'string' ? opts.search : '';
    let regex = new RegExp(search, 'i');

    let searchData = { $regex: regex };
    let sort = opts.sortBy;

    const options = {
      page: opts.page ? opts.page : 1,
      limit: opts.limit ? (opts.limit > 50 ? 50 : opts.limit) : 10,
      sort: sort || '-createdDate'
    };
    let id: ID = opts.iotDeviceId;
    let match = {
      $match: {
        // $or: [{ 'customer.companyName': searchData, 'operator.name': searchData, 'iotDevice.deviceNumber': searchData }]
        $or: [{ 'customerName': searchData }, { 'customerCode': searchData }, { 'connote.connoteNumber': searchData }]
      }
    };

    let lookup1: object = {
      $lookup: {
        from: 'connote',
        localField: '_id',
        foreignField: 'packageId',
        as: 'connote'
      }
    };

    let lookup2: object = {
      $lookup: {
        from: 'koli',
        localField: '_id',
        foreignField: 'pacakgeId',
        as: 'koliData'
      }
    };

    const aggregate = this.db.Package.aggregate([
      lookup1,
      {
        $unwind: {
          path: '$connote',
          preserveNullAndEmptyArrays: true
        }
      },
      lookup2,
      match
    ]);

    return await this.db.Package.aggregatePaginate(aggregate, options);
  };
}

export default new PackageServices();
