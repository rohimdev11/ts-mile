import Config from '../configs/config';
const bcrypt = require('bcryptjs');
const totp = require('totp-generator');
const saltRounds = Config.password.saltRounds;
const totpKey = Config.password.totp;
const generator = require('generate-password');

const Password = {
    randomPassword: (): any => {
        let password = generator.generate({
            length: 8,
            numbers: true,
            symbols: false,
            uppercase: true
        });
        return password;
    },
    generatePassword: (password: string): any => {
        let passwordHash = bcrypt.hashSync(password, saltRounds);
        return passwordHash;
    },
    generateToken: (): any => {
        const token = totp(totpKey, {
            digits: 6,
            algorithm: 'SHA-512',
            period: 60
        });
        return token;
    }
};

export default Password;
