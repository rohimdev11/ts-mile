import jwt from 'jsonwebtoken';
import Config from '../configs/config';
const secret_key = Config.jwt.secret_key;
import { IUserLogin } from '../interfaces/modules/userLogin';
import { IRefreshToken } from '../interfaces/modules/refreshToken';

import RefreshTokenServices from '../services/modules/refresh-token';

import { v4 as uuidv4 } from 'uuid';

import UserLoginServices from './../services/modules/user-login';

const JWT = {
    createToken: async (filter: any, user: any) => {
        let filters = {
            user: filter.user,
            ipAddress: filter.ipAddress,
            device: filter.device,
            tokenDeleted: filter.tokenDeleted
        };

        let userLogins = await UserLoginServices.find(filters);
        const tokenId = uuidv4();

        if (userLogins.length > 0) {
            for (const userLogin of userLogins) {
                userLogin.loggedOut = true;
                userLogin.tokenDeleted = true;
                await UserLoginServices.update(userLogin._id, userLogin);
                await UserLoginServices.deleteWhere({ tokenId: userLogin.tokenId }, userLogin);
                await RefreshTokenServices.deleteWhere({ tokenId: userLogin.tokenId });
            }
        }

        let userLogin = <IUserLogin>{
            user: user.id,
            tokenId: tokenId,
            ipAddress: filter.ipAddress,
            loggedInAt: new Date(),
            device: filter.device,
            tokenDeleted: false
        };
        userLogin = await UserLoginServices.create(userLogin);

        const options = { expiresIn: Config.jwt.expires };
        const payload = {
            id: user.id,
            nip: user.nip,
            name: user.name,
            email: user.email,
            tokenId: tokenId
        };
        const accessToken = await jwt.sign(payload, Config.jwt.secret_key, options);

        return {
            accessToken: accessToken,
            tokenId: tokenId,
            id: userLogin.user
        };
    },
    createRefreshToken: async (tokenId: any, id: any) => {
        const payload = { tokenId: tokenId, id: id };
        const options = { expiresIn: '1y' };

        const jwtToken = await jwt.sign(payload, Config.jwt.secret_key, options);
        let refreshToken;
        if (jwtToken) {
            refreshToken = <IRefreshToken>{
                tokenId: tokenId,
                token: jwtToken
            };
            refreshToken = await RefreshTokenServices.create(refreshToken);
        }

        return refreshToken;
    },
    verifyToken: (token: string): any => {
        return jwt.verify(token, secret_key);
    },
    decodeToken: (token: string): any => {
        return jwt.decode(token);
    },
    destroyToken: (token: string): any => {}
};

export default JWT;
