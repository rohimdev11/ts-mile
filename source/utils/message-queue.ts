import amqp from 'amqplib/callback_api';
import config from '../configs/config';

const MessageQueue = {
    sendQueue: function (queueName: string, data: any) {
        amqp.connect(config.integration.MQ, function (error0, connection) {
            if (error0) {
                console.log(error0);
                throw error0;
            }
            connection.createChannel(function (error1, channel) {
                if (error1) {
                    console.log(error1);
                    throw error1;
                }
                channel.assertQueue(queueName, {
                    durable: true
                });
                channel.sendToQueue(queueName, Buffer.from(JSON.stringify(data)), {
                    persistent: true
                });
                console.log(" [x] Sent '%s' '%s'", queueName, data);
            });
            setTimeout(function () {
                connection.close();
            }, 500);
        });
    }
};

export default MessageQueue;
